//
//  ViewController.swift
//  poke3d
//
//  Created by abdo emad  on 13/12/2023.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
//        sceneView.autoenablesDefaultLighting = true
        // Create a new scene
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARImageTrackingConfiguration()
        
        if let imageToTracke = ARReferenceImage.referenceImages(inGroupNamed: "AR image", bundle: .main){
            
            configuration.trackingImages = imageToTracke
            
            configuration.maximumNumberOfTrackedImages = 2
            
            print("sucessufl")
        }

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        
        let node = SCNNode()
        if let imageAnchor = anchor as? ARImageAnchor {
            let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
            
            plane.firstMaterial?.diffuse.contents = UIColor(white: 1.0, alpha: 0.5)
            
            
            let planeNode = SCNNode(geometry: plane)
            
            planeNode.eulerAngles.x = -.pi / 2
            
            node.addChildNode(planeNode)
            
            let theCardname = imageAnchor.referenceImage.name
            
            if let pokeScene = SCNScene(named: "art.scnassets/\(theCardname!).scn"){
                
                if let pokeNode = pokeScene.rootNode.childNodes.first{
                    

                    planeNode.addChildNode(pokeNode)
                }
            }
         
        }
        
        
        return node
    }
}
